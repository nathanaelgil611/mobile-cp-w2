import { Component, OnInit } from '@angular/core';
import { FirstPage } from '../first_page/first_page';
import { NavController } from 'ionic-angular';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{
  firstPage = FirstPage;

  

  constructor(public navCtrl: NavController) {
    
  }

  ngOnInit(){
  }

  onButtonPressed(){
    this.navCtrl.push(FirstPage, {nama: 'Nathan', umur: 21});
  }
  
  

}
