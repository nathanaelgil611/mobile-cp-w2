import { Component } from "@angular/core";
import { HomePage } from "../home/home";
import { NavController } from "ionic-angular";



@Component({
  selector: 'page-page-second',
  templateUrl: 'page-second.html',
})
export class PageSecondPage {
  homePage = HomePage;


  constructor(public navCtrl: NavController){}

  backToHome() {
    this.navCtrl.setRoot(HomePage);
  }

}
