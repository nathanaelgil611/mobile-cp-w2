import { Component, OnInit } from '@angular/core';
import { PageSecondPage } from '../page-second/page-second';
import { NavController, NavParams } from 'ionic-angular';


@Component({
    selector: 'page-sign-in',
    templateUrl: 'first_page.html',
})

export class FirstPage implements OnInit{
    secondPage = PageSecondPage;
    params:{nama:string, umur:number};


    constructor(public navCtrl: NavController, public navParams: NavParams){

    }

    ngOnInit(){
        this.params = this.navParams.data;
        console.log(this.navParams.get('nama') +", "+ this.navParams.get('umur') + "tahun.");
    }
    ionViewDidLoad(){
        // console.log("LALALALA");
    }
    
}