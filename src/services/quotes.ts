import { Quote } from '../data/quote.interface';
import { Injectable, Component } from '@angular/core';

@Injectable()
@Component({})
export class QuotesService {
    private favoriteQuotes: Quote[] = [];

    addQuoteToFavorites(quote: Quote) {
        this.favoriteQuotes.push(quote);   
        console.log(this.favoriteQuotes);

    }

    removeQuoteFromFavorites(quote: Quote) {
        const index: number = this.favoriteQuotes.indexOf(quote);
            if (index !== -1) {
                this.favoriteQuotes.splice(index, 1);
            }  
    }
    getAllFavoriteQuotes() {
        return this.favoriteQuotes;
        // console.log(this.favoriteQuotes);
    }
    isFavorite(quote: Quote) {}
}