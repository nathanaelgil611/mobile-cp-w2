import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertCmp, AlertController } from 'ionic-angular';
import { Quote } from '../../data/quote.interface';
import quotes from '../../data/quotes';
import { QuotesService } from '../../services/quotes';

/**
 * Generated class for the ListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage implements OnInit{
  quotes:Quote[];
  category: string;
  favQuotes:Quote[];
  textView:string[];

  constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl: AlertController, public quotesService: QuotesService) {
  }

  ngOnInit(){
    this.quotes = this.navParams.get('selectedQuotes');
    this.category = this.navParams.get('selectedCategory');
    this.favQuotes = this.quotesService.getAllFavoriteQuotes();
    // this.textView = "Favorite";
    console.log(this.favQuotes);
    // AlertController
  }

  onShowAlert(quote: Quote){
    for(let fav of this.favQuotes){
      console.log(fav.id);
      
      if(fav == quote){
        console.log("UDAH PERNAH!");
        this.textView[fav.id] = "1";
      }
    }
    const alert = this.alertCtrl.create({
      title: 'Add Quote',
      message: 'Are you sure you want to add the quote to favorites',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
              console.log("YES is clicked.");
              this.quotesService.addQuoteToFavorites(quote);
              // this.textView = "Unfavorite";
            }
          },
           {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log("NO is clicked.");
            }
          }
        ]
        });
        alert.present();
        }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListPage');
  }

}
