import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Quote } from '../../data/quote.interface';
import { QuotesService } from '../../services/quotes';

/**
 * Generated class for the FavoritePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favorite',
  templateUrl: 'favorite.html',
})
export class FavoritePage implements OnInit {

  favQuotes:Quote[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public quotesService: QuotesService) {
  }

  ngOnInit(){
    this.favQuotes = this.quotesService.getAllFavoriteQuotes();
    console.log(this.favQuotes);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritePage');
  }

}
